variables:
  TF_ROOT: ${CI_PROJECT_DIR}/infrastructure
  TF_STATE_NAME: nodejs-monorepo
  TF_ADDRESS: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/nodejs-monorepo
  TF_VAR_AWS_ACCESS_KEY_ID: ${AWS_ACCESS_KEY_ID}
  TF_VAR_AWS_SECRET_ACCESS_KEY: ${AWS_SECRET_ACCESS_KEY}
  TF_VAR_git_sha: ${CI_COMMIT_SHORT_SHA}
  APPS_DIR: ${CI_PROJECT_DIR}/apps

stages:
  - provision
  - build
  - deploy

.collect-all-app-artifacts:
  before_script:
    - cd $APPS_DIR
    - |
      mkdir .artifacts
      for d in */ ; do
          cd $APPS_DIR/$d
          APP_NAME=$(jq -r .name package.json)
          jq "{name: .name, version: .version, repository: .metadata.repository, port: .metadata.port, url: .metadata.url, cpu: .metadata.cpu, memory: .metadata.memory, tasks: .metadata.tasks, healthcheck: .metadata.healthcheck, databaseEnabled: .metadata.databaseEnabled}" package.json > $APPS_DIR/.artifacts/$APP_NAME.json
      done
      cd $APPS_DIR/.artifacts
      jq -s . * | jq 'reduce .[] as $i ({}; .[$i.name] = $i)' > app-artifacts.json
  artifacts:
    paths:
      - $APPS_DIR/.artifacts/app-artifacts.json

.get-app-docker-info:
  before_script:
    - amazon-linux-extras install docker
    - yum -y install jq
    - docker --version
    - cd $APPS_DIR/$APP
    - |
      jq -r "{name: .name, version: .version, repository: .metadata.repository, registry: .metadata.registry}" package.json | jq -r 'to_entries[] | [.key,.value] | join("=")' > $APPS_DIR/$APP/docker.env
  artifacts:
    paths:
      - $APPS_DIR/$APP/docker.env

.build-app:
  script:
    - cd ${CI_PROJECT_DIR}/apps/$APP
    - echo $CI_COMMIT_SHORT_SHA
    - cat $APPS_DIR/$APP/docker.env
    - export $(xargs < $APPS_DIR/$APP/docker.env)
    - docker build --tag $registry/$repository:$CI_COMMIT_SHORT_SHA --tag $registry/$repository:latest --tag $registry/$repository:$version --build-arg NODE_ENV=production .
    - aws ecr get-login-password | docker login --username AWS --password-stdin $registry
    - docker push $registry/$repository:$CI_COMMIT_SHORT_SHA
    - docker push $registry/$repository:$version
    - docker push $registry/$repository:latest

.terraform-apply:
  script:
    - cd ${CI_PROJECT_DIR}/infrastructure
    - cat $APPS_DIR/.artifacts/app-artifacts.json
    - gitlab-terraform init
    - export TF_VAR_artifacts_file=$APPS_DIR/.artifacts/app-artifacts.json
    - gitlab-terraform plan
    - gitlab-terraform apply -auto-approve
    - gitlab-terraform output -json

###
### Jobs
###

# Infrastructure provisioning (for initial setup and infrastructure changes)
provision-infrastructure:
  image:
    name: "$CI_TEMPLATE_REGISTRY_HOST/gitlab-org/terraform-images/releases/1.4:latest"
  stage: provision
  extends: [.collect-all-app-artifacts, .terraform-apply]
  rules:
    - changes:
      - infrastructure/**
  resource_group: ${TF_STATE_NAME}

# Web build
build:web:
  stage: build
  variables:
    APP: web
    DOCKER_HOST: tcp://docker:2375
  extends: [.get-app-docker-info, .build-app]
  rules:
    - changes:
      - apps/web/*  
  image: 
    name: amazon/aws-cli
    entrypoint: [""]
  services:
    - docker:dind

# API build
build:api:
  stage: build
  variables:
    APP: api
    DOCKER_HOST: tcp://docker:2375    
  extends: [.get-app-docker-info, .build-app]
  rules:
    - changes:
      - apps/api/*
  image: 
    name: amazon/aws-cli
    entrypoint: [""]
  services:
    - docker:dind

# Deploy apps
deploy:
  stage: deploy
  image:
    name: "$CI_TEMPLATE_REGISTRY_HOST/gitlab-org/terraform-images/releases/1.4:latest"  
  extends: [.collect-all-app-artifacts, .terraform-apply]
  resource_group: ${TF_STATE_NAME}
  rules:
    - changes:
      - apps/**/*  
  needs:
    - job: build:api
      optional: true
    - job: build:web
      optional: true