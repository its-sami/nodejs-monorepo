terraform {
  backend "http" {
  }
}

provider "aws" {
  region  = var.aws_region
}

# Required to deploy certificate to us-east-1 region for Cloudfront 
provider "aws" {
  alias  = "us-east-1"
  region = "us-east-1"
}