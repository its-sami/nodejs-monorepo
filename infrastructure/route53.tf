###
### Route53
###
data "aws_route53_zone" "root" {
  name = var.root_domain_name
}

# Route53 Domains
resource "aws_route53_record" "a-record" {
  name    = var.root_domain_name
  zone_id = data.aws_route53_zone.root.zone_id
  type    = "A"
  alias {

    # Traffic to cloudfront distribution
    name                   = aws_cloudfront_distribution.cf-distribution.domain_name
    zone_id                = aws_cloudfront_distribution.cf-distribution.hosted_zone_id    
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "cname-wildcard" {
  name    = "*"
  zone_id = data.aws_route53_zone.root.zone_id
  type    = "CNAME"
  ttl     = 300

  weighted_routing_policy {
    weight = 10
  }  

  set_identifier = "*"
  records        = [var.root_domain_name]
}

# Certificate
resource "aws_acm_certificate" "certificate" {
  domain_name               = var.root_domain_name
  subject_alternative_names = [
    "*.${var.root_domain_name}"
  ]
  validation_method         = "DNS"
  tags                      = {
    Name = "${var.project}-certificate"
  }
}

resource "aws_route53_record" "certvalidation" {
  name =  tolist(aws_acm_certificate.certificate.domain_validation_options)[0].resource_record_name
  records = [tolist(aws_acm_certificate.certificate.domain_validation_options)[0].resource_record_value]
  type = tolist(aws_acm_certificate.certificate.domain_validation_options)[0].resource_record_type

  allow_overwrite = true
  ttl             = 60
  zone_id         = data.aws_route53_zone.root.zone_id
}

resource "aws_acm_certificate_validation" "certvalidation" {
  certificate_arn         = aws_acm_certificate.certificate.arn
  validation_record_fqdns = [aws_route53_record.certvalidation.fqdn]
}

# US certificate required for cloudfront
resource "aws_acm_certificate" "us_certificate" {
  provider = aws.us-east-1

  domain_name               = var.root_domain_name
  subject_alternative_names = [
    "*.${var.root_domain_name}"
  ]
  validation_method         = "DNS"
  tags                      = {
    Name = "${var.project}-certificate"
  }
}

resource "aws_route53_record" "us_certvalidation" {
  provider = aws.us-east-1

  name =  tolist(aws_acm_certificate.us_certificate.domain_validation_options)[0].resource_record_name
  records = [tolist(aws_acm_certificate.us_certificate.domain_validation_options)[0].resource_record_value]
  type = tolist(aws_acm_certificate.us_certificate.domain_validation_options)[0].resource_record_type

  allow_overwrite = true
  ttl             = 60
  zone_id         = data.aws_route53_zone.root.zone_id
}

resource "aws_acm_certificate_validation" "us_certvalidation" {
  provider = aws.us-east-1

  certificate_arn         = aws_acm_certificate.us_certificate.arn
  validation_record_fqdns = [aws_route53_record.us_certvalidation.fqdn]
}