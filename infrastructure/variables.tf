###
### AWS
###
variable "AWS_ACCESS_KEY_ID" {
  sensitive = true
  description = "AWS access key from project IAM user"
}

variable "AWS_SECRET_ACCESS_KEY" {
  sensitive = true
  description = "AWS secret from project IAM user"
}

variable "aws_region" {
  description = "The AWS region to create things in."
  default     = "ap-southeast-2"
}


###
### Project and System 
###

variable "project" {
  description = "Name of the project as a whole."
  default     = "nodejs-monorepo"
}

variable "root_domain_name" {
  default     = ""
  description = "Platform domain name (subdomains can be created underneath this)"
}

variable "vpc_cidr" {
  description = "CIDR for the VPC"
  default     = "172.17.0.0/16"
}

variable "az_count" {
  description = "Number of AZs to cover in a given AWS region"
  default     = "2"
}

variable git_sha {
  description = "Current Git commit SHA"
  type        = string
}


###
### Fargate
###
variable "fargate-task-service-role" {
  description = "Name of the stack."
}


###
### DATABASE
###
variable "db_instance_type" {
  description = "RDS instance type"
  default     = "db.t3.micro"
}

variable "db_name" {
  description = "RDS DB name"
  default     = "nodejs_monorepo"
}

variable "db_user" {
  description = "RDS DB username"
  default     = "nodejs_monorepo"
}

variable "db_profile" {
  description = "RDS Profile"
  default     = "mysql"
}


###
### Cloud Watch
###
variable "cw_log_group" {
  description = "CloudWatch Log Group"
  default     = "nodejs-monorepo"
}

variable "cw_log_stream" {
  description = "CloudWatch Log Stream"
  default     = "fargate"
}

###
### Microservice artifacts
###
variable artifacts_file {
  type        = string
  description = "List of microservices with their respective artifacts"
}