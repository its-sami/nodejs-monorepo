###
### RDS
###

# Generate random password for database
resource "random_string" "database-password" {
  length  = 32
  upper   = true
  numeric = true
  special = false
}

resource "aws_db_instance" "db" {

  identifier             = var.project
  db_name                = var.db_name
  username               = var.db_user
  password               = random_string.database-password.result
  instance_class         = var.db_instance_type
  allocated_storage      = 5
  engine                 = "postgres"
  engine_version         = "15.3"
  availability_zone      = "${var.aws_region}a"
  vpc_security_group_ids = [aws_security_group.db-sg.id]
  multi_az               = false
  db_subnet_group_name   = aws_db_subnet_group.db-subnet-grp.id
  parameter_group_name   = "default.postgres15"
  publicly_accessible    = false
  skip_final_snapshot    = true

  backup_retention_period = 7

  tags = {
    Name = "${var.project}-db"
  }
}

resource "aws_db_subnet_group" "db-subnet-grp" {
  name        = "db-sgrp"
  description = "Database Subnet Group"
  subnet_ids  = aws_subnet.private.*.id
}

output "db_host" {
  value = aws_db_instance.db.address
}

output "db_port" {
  value = aws_db_instance.db.port
}

output "db_url" {
  value = "${aws_db_instance.db.address}/${var.db_name}"
}