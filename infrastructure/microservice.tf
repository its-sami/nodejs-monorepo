###
### ALB
###
resource "aws_alb_target_group" "trgp" {
  for_each    = local.apps

  name        = "${replace(each.value.name, "_", "-")}-trgp"
  port        = each.value.port
  protocol    = "HTTP"
  vpc_id      = aws_vpc.main.id
  target_type = "ip"

  health_check {
    path      = each.value.healthcheck
  }
}

# ALB listener rule to pass api traffic to api target group
resource "aws_lb_listener_rule" "host-listener" {
  for_each     = local.apps

  listener_arn = aws_alb_listener.alb-listener.arn
  priority     = each.value.port

  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.trgp[each.key].arn
  }

  condition {
    host_header {
      values = [each.value.url]
    }
  }
}


###
### ECR 
###
resource "aws_ecr_repository" "image_repo" {
  for_each             = local.apps

  name                 = each.value.repository
  image_tag_mutability = "MUTABLE"
}

output "image_repo_url" {
  value = {for k, v in aws_ecr_repository.image_repo: k => v.repository_url}
}


###
### ECS
###
resource "aws_ecs_task_definition" "task-def" {
  for_each                 = local.apps

  family                   = each.value.name
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = each.value.cpu
  memory                   = each.value.memory
  execution_role_arn       = aws_iam_role.tasks-service-role.arn

  container_definitions = jsonencode([
    {
      name        = each.value.name
      image       = "${aws_ecr_repository.image_repo[each.key].repository_url}:${each.value.version}"
      cpu         = "${tonumber(each.value.cpu)}"
      memory      = "${tonumber(each.value.memory)}"
      networkMode = "awsvpc"

      logConfiguration = {
        logDriver = "awslogs"
        options   = {
          awslogs-group         = var.cw_log_group
          awslogs-region        = var.aws_region
          awslogs-stream-prefix = var.cw_log_stream
        }
      }
      
      portMappings = [
        {
          containerPort = "${tonumber(each.value.port)}"
          hostPort      = "${tonumber(each.value.port)}"
        }
      ]

      environment = [
        {
            name  = "PORT"
            value = "${tostring(each.value.port)}"
        },
        {
            name  = "API_HOST"
            value = "https://${local.apps.devops_api.url}"
        },        
        {
            name  = "DB"
            value = each.value.databaseEnabled == "true" ? var.db_name : ""
        },
        {
            name  = "DBUSER"
            value = each.value.databaseEnabled == "true" ? var.db_user : ""
        },
        {
            name  = "DBPASS"
            value = each.value.databaseEnabled == "true" ? random_string.database-password.result : ""
        },
        {
            name  = "DBHOST"
            value = each.value.databaseEnabled == "true" ? aws_db_instance.db.address : ""
        },
        {
            name  = "DBPORT"
            value = each.value.databaseEnabled == "true" ? "${tostring(aws_db_instance.db.port)}" : ""
        }
      ]
    }
  ])
}

resource "aws_ecs_service" "service" {
  for_each        = local.apps

  name            = "${var.project}-${replace(each.value.name, "_", "-")}-service"
  cluster         = aws_ecs_cluster.ecs-cluster.id
  task_definition = aws_ecs_task_definition.task-def[each.key].arn
  desired_count   = each.value.tasks
  launch_type     = "FARGATE"

  network_configuration {
    security_groups = [aws_security_group.task-sg[each.key].id]
    subnets         = aws_subnet.private.*.id
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.trgp[each.key].id
    container_name   = each.value.name
    container_port   = each.value.port
  }

  depends_on = [
    aws_alb_listener.alb-listener,
  ]
}

resource "aws_security_group" "task-sg" {
  for_each    = local.apps

  name        = "${var.project}-${replace(each.value.name, "_", "-")}-task-sg"
  description = "Allow inbound access to ${each.value.name} tasks from the ALB only"
  vpc_id      = aws_vpc.main.id

  ingress {
    protocol        = "tcp"
    from_port       = each.value.port
    to_port         = each.value.port
    security_groups = [aws_security_group.alb-sg.id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "${var.project}-${each.value.name}-task-sg"
  }
}