###
### Security groups
###

# ALB allow from port 80 and 443
resource "aws_security_group" "alb-sg" {
  name        = "${var.project}-alb-sg"
  description = "ALB Security Group"
  vpc_id      = aws_vpc.main.id

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }  

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "${var.project}-alb-sg"
  }
}

# RDS allow port 5432 from private network
resource "aws_security_group" "db-sg" {
  name        = "${var.project}-db-sg"
  description = "Access to the RDS instances from private subnets in the VPC"
  vpc_id      = aws_vpc.main.id

  dynamic "ingress" {
    for_each = aws_subnet.private
    content {
      from_port   = 5432 # Default postgres port
      to_port     = 5432 # Default postgres port
      protocol    = "tcp"
      cidr_blocks = [ingress.value.cidr_block]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.project}-db-sg"
  }
}