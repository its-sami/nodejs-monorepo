# 3 Tier NodeJS Application and Infrastructure

This repository builds and deploys both the AWS infrastructure and application files for a 3-tiered NodeJS application.

The infrastructure consists of an ECS cluster with services for each application and task definitions specifying the application's Docker image. An RDS deployed on a private subnet and communication to it is only enabled across the VPC's private subnets.

All services are deployed onto private subnets and traffic is routed to services through an Internet Gateway and Load Balancer.

A CloudFront CDN is placed in front of the services for content distribution across different regions.

[Network Architecture Diagram](./.diagrams/network-architecture.png)

## Prerequisits

**Route53 Hosted Zone**

This project requires a hosted zone to be setup prior to the pipeline's being run. This ensures DNS resolution and certificate generation can be run successfuly. 

**Store IAM user credentials as Gitlab variables**

An IAM user is needed to interact with AWS. If you want to generate a new IAM user and credentials run the `./create-iam-user.sh` script.

Upload the following variables as Gitlab variables:
- AWS_ACCESS_KEY_ID
- AWS_DEFAULT_REGION
- AWS_SECRET_ACCESS_KEY

## Getting Started

**Provision Core Infrastructure**

You must first provision the core infrastructure and generate the ECR repositories for your applications.

1. Make a change in the `infrastructure` directory (to ensure the pipeline will build the provision-infrastructure job)
2. Commit and push
4. Copy the image_repo_url output and add it into the metadata for your app as `"registry": "my-app-registry.com"` (see below for an example of application metadata)

**Deploy Applications**

1. Define your application's metadata in the `package.json` file in the root of the app (see below for an example of application metadata)
2. Commit and push

> This process is only required for the initial deployment. Once this has been completed any changes to the infrastructure or applications will be managed automatically in the pipeline

## Deployments

This repository is setup with CI/CD pipeline that enables automatic deployments. Everytime a change is detected in an application directory a docker image is built and tagged. 

Currently deployments will only go out when a new version is promoted in the `package.json` file.

Alternatively, the task definition can be changed to utilise the Git SHA tag, which would ensure every commit is deployed.


## Application Metadata

The `metadata` block in the `package.json` file is used to define deployment information. This block enables changes to application resources such as CPU and Memory, instance scaling with the tasks variable, port specification, application URL, and more. It is also where the Docker registry and repository values are stored.

Example:

```
"metadata": {
    "repository": "devops_web",
    "registry": "1234567890.dkr.ecr.ap-southeast-2.amazonaws.com",
    "port": "8080",
    "url": "fatherbean.com",
    "cpu": "1024",
    "memory": "2048",
    "tasks": "3",
    "healthcheck": "/",
    "databaseEnabled": "false"
}
```

## Limitations and Pitfalls

**Image Deployment Using Terraform**

Some might argue that a deployment of an application should not be done using Terraform as it should only be used for infrastructure changes, which I somewhat agree with.

Because the ECS services and task definitions are created using Terraform it makes sense in this project to use Terraform as the deployment method. As modifying the task definitions in another way would cause the Terraform state to become out of sync.

We might be able to impliment a tool like [ecs-deploy](https://github.com/silinternational/ecs-deploy) to handle changes to task definitions, though the Terraform state will still need to be updated.

**Terraform Isolation**

This project contains all of the infrastructure files used to deploy the entire solution. This includes core infrastructure like the VPC, subnets, and more. This can introduce a security risk as developers that may not have sufficient knowledge about AWS infrastructure might change the files and cause the entire system to go down.

As a remedy to this I would move the core infrastructure into a different repository and have it provisioned there. Then the microservice specific infrastructure can remain in this project and reference the Terraform state of the core infrastructure. This would ensure that changes in this repository would be isolated to only the applications. 

**Manual Intervention**

Currently there is a manual step needed to copy the ECR registries into the application's metadata. Because we require the registries to be created before pushing Docker images this needs to happen in it's own step.

This could be automated by taking the output from the Terraform provision job and committing it to the app metadata automatically. At the moment I am prioritising other tasks but would like to come back to this. I am just making a note of this for now.